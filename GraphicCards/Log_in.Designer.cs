﻿namespace GraphicCards
{
    partial class Log_in
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Log_in));
            this.textBox_login = new System.Windows.Forms.TextBox();
            this.textBox_pass = new System.Windows.Forms.TextBox();
            this.pictureBox_showpass = new System.Windows.Forms.PictureBox();
            this.pictureBox_hidepass = new System.Windows.Forms.PictureBox();
            this.linkLabel_onreg = new System.Windows.Forms.LinkLabel();
            this.button_log = new System.Windows.Forms.Button();
            this.label_signup = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel_login = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_log_exit = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_showpass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_hidepass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel_login.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox_login
            // 
            this.textBox_login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(201)))), ((int)(((byte)(202)))));
            this.textBox_login.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_login.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_login.Location = new System.Drawing.Point(123, 170);
            this.textBox_login.Multiline = true;
            this.textBox_login.Name = "textBox_login";
            this.textBox_login.Size = new System.Drawing.Size(245, 28);
            this.textBox_login.TabIndex = 2;
            this.textBox_login.Text = "Username";
            this.textBox_login.Enter += new System.EventHandler(this.textBox_login_Enter);
            this.textBox_login.Leave += new System.EventHandler(this.textBox_login_Leave);
            // 
            // textBox_pass
            // 
            this.textBox_pass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(201)))), ((int)(((byte)(202)))));
            this.textBox_pass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_pass.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.textBox_pass.Location = new System.Drawing.Point(123, 235);
            this.textBox_pass.Multiline = true;
            this.textBox_pass.Name = "textBox_pass";
            this.textBox_pass.Size = new System.Drawing.Size(245, 28);
            this.textBox_pass.TabIndex = 3;
            this.textBox_pass.Text = "Password";
            this.textBox_pass.Enter += new System.EventHandler(this.textBox_pass_Enter);
            this.textBox_pass.Leave += new System.EventHandler(this.textBox_pass_Leave);
            // 
            // pictureBox_showpass
            // 
            this.pictureBox_showpass.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox_showpass.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_showpass.Image")));
            this.pictureBox_showpass.Location = new System.Drawing.Point(374, 235);
            this.pictureBox_showpass.Name = "pictureBox_showpass";
            this.pictureBox_showpass.Size = new System.Drawing.Size(36, 33);
            this.pictureBox_showpass.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_showpass.TabIndex = 7;
            this.pictureBox_showpass.TabStop = false;
            this.pictureBox_showpass.Click += new System.EventHandler(this.pictureBox_showpass_Click);
            // 
            // pictureBox_hidepass
            // 
            this.pictureBox_hidepass.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox_hidepass.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_hidepass.Image")));
            this.pictureBox_hidepass.Location = new System.Drawing.Point(374, 235);
            this.pictureBox_hidepass.Name = "pictureBox_hidepass";
            this.pictureBox_hidepass.Size = new System.Drawing.Size(36, 33);
            this.pictureBox_hidepass.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_hidepass.TabIndex = 8;
            this.pictureBox_hidepass.TabStop = false;
            this.pictureBox_hidepass.Click += new System.EventHandler(this.pictureBox_hidepass_Click);
            // 
            // linkLabel_onreg
            // 
            this.linkLabel_onreg.AutoSize = true;
            this.linkLabel_onreg.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel_onreg.Location = new System.Drawing.Point(198, 382);
            this.linkLabel_onreg.Name = "linkLabel_onreg";
            this.linkLabel_onreg.Size = new System.Drawing.Size(95, 13);
            this.linkLabel_onreg.TabIndex = 9;
            this.linkLabel_onreg.TabStop = true;
            this.linkLabel_onreg.Text = "Зареєструватись";
            this.linkLabel_onreg.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_onreg_LinkClicked);
            // 
            // button_log
            // 
            this.button_log.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(246)))), ((int)(((byte)(242)))));
            this.button_log.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_log.FlatAppearance.BorderSize = 0;
            this.button_log.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_log.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_log.ForeColor = System.Drawing.Color.Black;
            this.button_log.Location = new System.Drawing.Point(157, 319);
            this.button_log.Name = "button_log";
            this.button_log.Size = new System.Drawing.Size(175, 37);
            this.button_log.TabIndex = 10;
            this.button_log.Text = "Увійти";
            this.button_log.UseVisualStyleBackColor = false;
            this.button_log.Click += new System.EventHandler(this.button_log_Click);
            // 
            // label_signup
            // 
            this.label_signup.AutoSize = true;
            this.label_signup.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_signup.ForeColor = System.Drawing.Color.Black;
            this.label_signup.Location = new System.Drawing.Point(175, 36);
            this.label_signup.Name = "label_signup";
            this.label_signup.Size = new System.Drawing.Size(156, 30);
            this.label_signup.TabIndex = 2;
            this.label_signup.Text = "Авторизація";
            this.label_signup.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(18, 25);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(49, 49);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // panel_login
            // 
            this.panel_login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(201)))), ((int)(((byte)(202)))));
            this.panel_login.Controls.Add(this.panel1);
            this.panel_login.Controls.Add(this.button_log_exit);
            this.panel_login.Controls.Add(this.pictureBox1);
            this.panel_login.Controls.Add(this.label_signup);
            this.panel_login.Location = new System.Drawing.Point(-6, -13);
            this.panel_login.Name = "panel_login";
            this.panel_login.Size = new System.Drawing.Size(567, 82);
            this.panel_login.TabIndex = 0;
            this.panel_login.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_login_MouseDown);
            this.panel_login.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_login_MouseMove);
            this.panel_login.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel_login_MouseUp);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(3, 81);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(494, 1);
            this.panel1.TabIndex = 18;
            // 
            // button_log_exit
            // 
            this.button_log_exit.FlatAppearance.BorderSize = 0;
            this.button_log_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_log_exit.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_log_exit.ForeColor = System.Drawing.Color.Black;
            this.button_log_exit.Location = new System.Drawing.Point(467, 13);
            this.button_log_exit.Name = "button_log_exit";
            this.button_log_exit.Size = new System.Drawing.Size(30, 31);
            this.button_log_exit.TabIndex = 6;
            this.button_log_exit.Text = "X";
            this.button_log_exit.UseVisualStyleBackColor = true;
            this.button_log_exit.Click += new System.EventHandler(this.button_log_exit_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(69, 161);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(48, 46);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 11;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(73, 225);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(40, 38);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 12;
            this.pictureBox3.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(71, 206);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(297, 1);
            this.panel2.TabIndex = 13;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(73, 269);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(297, 1);
            this.panel3.TabIndex = 14;
            // 
            // Log_in
            // 
            this.AcceptButton = this.button_log;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(201)))), ((int)(((byte)(202)))));
            this.ClientSize = new System.Drawing.Size(490, 414);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox_showpass);
            this.Controls.Add(this.button_log);
            this.Controls.Add(this.linkLabel_onreg);
            this.Controls.Add(this.pictureBox_hidepass);
            this.Controls.Add(this.textBox_pass);
            this.Controls.Add(this.textBox_login);
            this.Controls.Add(this.panel_login);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Log_in";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sign in";
            this.Load += new System.EventHandler(this.Login_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_showpass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_hidepass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel_login.ResumeLayout(false);
            this.panel_login.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBox_login;
        private System.Windows.Forms.TextBox textBox_pass;
        private System.Windows.Forms.PictureBox pictureBox_showpass;
        private System.Windows.Forms.PictureBox pictureBox_hidepass;
        private System.Windows.Forms.LinkLabel linkLabel_onreg;
        private System.Windows.Forms.Button button_log;
        private System.Windows.Forms.Label label_signup;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel_login;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button button_log_exit;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel1;
    }
}