﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GraphicCards
{
    public partial class Registration : Form
    {

        Database database = new Database();

        public Registration()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Registration_Load(object sender, EventArgs e)
        {
            textBox_regpass.PasswordChar = '*';
            pictureBox_hidepass.Visible = false;

        }

        private void button_regist_Click(object sender, EventArgs e)
        {
            

            string loginreg = textBox_reglogin.Text;
            string passreg = textBox_regpass.Text;

            string sqlquery = $"insert into registration(login_user, password_user,is_admin) values ('{loginreg}', '{passreg}', 0) ";
            SqlCommand cmd = new SqlCommand(sqlquery, database.getConnection());

            database.openConnection();
            if (checkuser())
            {
                if (cmd.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("Аккаунт успішно створено!", "Успіх!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Log_in logform = new Log_in();
                    this.Hide();
                    logform.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Аккаунт не створено!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                database.closeConnection();
            }

        }

        private bool checkuser()
        {
            string checkloginreg = textBox_reglogin.Text;
            string checkpassreg = textBox_regpass.Text;

            SqlDataAdapter adapter = new SqlDataAdapter();
            DataTable dt = new DataTable();
            string sqlquery = $"select id_user, login_user, password_user, is_admin from registration where login_user = '{checkloginreg}' and password_user = '{checkpassreg}'";

            SqlCommand cmd = new SqlCommand(sqlquery, database.getConnection());

            adapter.SelectCommand = cmd;
            adapter.Fill(dt);

            if(dt.Rows.Count > 0)
            {
                MessageBox.Show("Такий запис вже існує!", "Помилка!");
                return false;
            }
            else
            {
                return true;
            }
        }

        private void pictureBox_showpass_Click(object sender, EventArgs e)
        {
            textBox_regpass.UseSystemPasswordChar = true;
            pictureBox_showpass.Visible = false;
            pictureBox_hidepass.Visible = true;
        }

        private void pictureBox_hidepass_Click(object sender, EventArgs e)
        {
            textBox_regpass.UseSystemPasswordChar = false;
            pictureBox_showpass.Visible = true;
            pictureBox_hidepass.Visible = false;
        }

        //Кастомна кнопка закриття вікна
        private void button_reg_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        //Placeholder для логіну та паролю форма Log_in
        private void textBox_reglogin_Enter_1(object sender, EventArgs e)
        {
            if (textBox_reglogin.Text == "Username")
            {
                textBox_reglogin.Text = "";
            }
        }

        private void textBox_reglogin_Leave_1(object sender, EventArgs e)
        {
            if (textBox_reglogin.Text == "")
            {
                textBox_reglogin.Text = "Username";
            }
        }

        private void textBox_regpass_Enter(object sender, EventArgs e)
        {
            textBox_regpass.UseSystemPasswordChar = false;
            if (textBox_regpass.Text == "Password")
            {
                textBox_regpass.Text = "";
            }
        }

        private void textBox_regpass_Leave(object sender, EventArgs e)
        {
            textBox_regpass.UseSystemPasswordChar = true;
            if (textBox_regpass.Text == "")
            {
                textBox_regpass.Text = "Password";
            }
        }

        //Кнопка повернутися до форми логину
        private void button_reg_back_Click(object sender, EventArgs e)
        {
            Log_in frm = new Log_in();
            frm.Show();
            this.Hide();
        }

        //Можливість переносити вікно форми
        bool mouseDown;
        private Point offset;

        private void panel_reg_MouseDown(object sender, MouseEventArgs e)
        {
            offset.X = e.X;
            offset.Y = e.Y;
            mouseDown = true;
        }

        private void panel_reg_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown == true)
            {
                Point currentScreenPos = PointToScreen(e.Location);
                Location = new Point(currentScreenPos.X - offset.X, currentScreenPos.Y - offset.Y);
            }
        }

        private void panel_reg_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }
    }
}
