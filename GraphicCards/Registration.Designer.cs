﻿namespace GraphicCards
{
    partial class Registration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Registration));
            this.panel_reg = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_reg_exit = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label_reg = new System.Windows.Forms.Label();
            this.textBox_reglogin = new System.Windows.Forms.TextBox();
            this.textBox_regpass = new System.Windows.Forms.TextBox();
            this.button_regist = new System.Windows.Forms.Button();
            this.pictureBox_showpass = new System.Windows.Forms.PictureBox();
            this.pictureBox_hidepass = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button_reg_back = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel_reg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_showpass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_hidepass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.button_reg_back)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_reg
            // 
            this.panel_reg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(201)))), ((int)(((byte)(202)))));
            this.panel_reg.Controls.Add(this.panel1);
            this.panel_reg.Controls.Add(this.button_reg_exit);
            this.panel_reg.Controls.Add(this.pictureBox1);
            this.panel_reg.Controls.Add(this.label_reg);
            this.panel_reg.Location = new System.Drawing.Point(-6, -13);
            this.panel_reg.Name = "panel_reg";
            this.panel_reg.Size = new System.Drawing.Size(567, 82);
            this.panel_reg.TabIndex = 0;
            this.panel_reg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_reg_MouseDown);
            this.panel_reg.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_reg_MouseMove);
            this.panel_reg.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel_reg_MouseUp);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(3, 82);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(494, 1);
            this.panel1.TabIndex = 17;
            // 
            // button_reg_exit
            // 
            this.button_reg_exit.FlatAppearance.BorderSize = 0;
            this.button_reg_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_reg_exit.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_reg_exit.ForeColor = System.Drawing.Color.Black;
            this.button_reg_exit.Location = new System.Drawing.Point(467, 13);
            this.button_reg_exit.Name = "button_reg_exit";
            this.button_reg_exit.Size = new System.Drawing.Size(30, 31);
            this.button_reg_exit.TabIndex = 14;
            this.button_reg_exit.Text = "X";
            this.button_reg_exit.UseVisualStyleBackColor = true;
            this.button_reg_exit.Click += new System.EventHandler(this.button_reg_exit_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(18, 25);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(49, 49);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label_reg
            // 
            this.label_reg.AutoSize = true;
            this.label_reg.Font = new System.Drawing.Font("Century Gothic", 18F);
            this.label_reg.ForeColor = System.Drawing.Color.Black;
            this.label_reg.Location = new System.Drawing.Point(110, 34);
            this.label_reg.Name = "label_reg";
            this.label_reg.Size = new System.Drawing.Size(306, 30);
            this.label_reg.TabIndex = 1;
            this.label_reg.Text = "Реєстрація користувача";
            this.label_reg.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // textBox_reglogin
            // 
            this.textBox_reglogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(201)))), ((int)(((byte)(202)))));
            this.textBox_reglogin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_reglogin.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.textBox_reglogin.Location = new System.Drawing.Point(123, 170);
            this.textBox_reglogin.Multiline = true;
            this.textBox_reglogin.Name = "textBox_reglogin";
            this.textBox_reglogin.Size = new System.Drawing.Size(245, 28);
            this.textBox_reglogin.TabIndex = 1;
            this.textBox_reglogin.Text = "Username";
            this.textBox_reglogin.Enter += new System.EventHandler(this.textBox_reglogin_Enter_1);
            this.textBox_reglogin.Leave += new System.EventHandler(this.textBox_reglogin_Leave_1);
            // 
            // textBox_regpass
            // 
            this.textBox_regpass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(201)))), ((int)(((byte)(202)))));
            this.textBox_regpass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_regpass.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.textBox_regpass.Location = new System.Drawing.Point(123, 235);
            this.textBox_regpass.Multiline = true;
            this.textBox_regpass.Name = "textBox_regpass";
            this.textBox_regpass.Size = new System.Drawing.Size(245, 28);
            this.textBox_regpass.TabIndex = 2;
            this.textBox_regpass.Text = "Password";
            this.textBox_regpass.Enter += new System.EventHandler(this.textBox_regpass_Enter);
            this.textBox_regpass.Leave += new System.EventHandler(this.textBox_regpass_Leave);
            // 
            // button_regist
            // 
            this.button_regist.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(246)))), ((int)(((byte)(242)))));
            this.button_regist.FlatAppearance.BorderSize = 0;
            this.button_regist.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_regist.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_regist.ForeColor = System.Drawing.Color.Black;
            this.button_regist.Location = new System.Drawing.Point(157, 319);
            this.button_regist.Name = "button_regist";
            this.button_regist.Size = new System.Drawing.Size(175, 37);
            this.button_regist.TabIndex = 3;
            this.button_regist.Text = "Зареєструватись";
            this.button_regist.UseVisualStyleBackColor = false;
            this.button_regist.Click += new System.EventHandler(this.button_regist_Click);
            // 
            // pictureBox_showpass
            // 
            this.pictureBox_showpass.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox_showpass.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_showpass.Image")));
            this.pictureBox_showpass.Location = new System.Drawing.Point(374, 235);
            this.pictureBox_showpass.Name = "pictureBox_showpass";
            this.pictureBox_showpass.Size = new System.Drawing.Size(36, 33);
            this.pictureBox_showpass.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_showpass.TabIndex = 8;
            this.pictureBox_showpass.TabStop = false;
            this.pictureBox_showpass.Click += new System.EventHandler(this.pictureBox_showpass_Click);
            // 
            // pictureBox_hidepass
            // 
            this.pictureBox_hidepass.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox_hidepass.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_hidepass.Image")));
            this.pictureBox_hidepass.Location = new System.Drawing.Point(374, 235);
            this.pictureBox_hidepass.Name = "pictureBox_hidepass";
            this.pictureBox_hidepass.Size = new System.Drawing.Size(36, 33);
            this.pictureBox_hidepass.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_hidepass.TabIndex = 9;
            this.pictureBox_hidepass.TabStop = false;
            this.pictureBox_hidepass.Click += new System.EventHandler(this.pictureBox_hidepass_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(69, 161);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(48, 46);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 12;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(73, 225);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(40, 38);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 13;
            this.pictureBox3.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(71, 206);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(297, 1);
            this.panel2.TabIndex = 14;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(73, 269);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(297, 1);
            this.panel3.TabIndex = 15;
            // 
            // button_reg_back
            // 
            this.button_reg_back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_reg_back.Image = ((System.Drawing.Image)(resources.GetObject("button_reg_back.Image")));
            this.button_reg_back.Location = new System.Drawing.Point(12, 376);
            this.button_reg_back.Name = "button_reg_back";
            this.button_reg_back.Size = new System.Drawing.Size(28, 26);
            this.button_reg_back.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.button_reg_back.TabIndex = 16;
            this.button_reg_back.TabStop = false;
            this.button_reg_back.Click += new System.EventHandler(this.button_reg_back_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Black;
            this.panel4.Location = new System.Drawing.Point(0, 69);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(500, 1);
            this.panel4.TabIndex = 17;
            // 
            // Registration
            // 
            this.AcceptButton = this.button_regist;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(201)))), ((int)(((byte)(202)))));
            this.ClientSize = new System.Drawing.Size(490, 414);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.button_reg_back);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox_showpass);
            this.Controls.Add(this.button_regist);
            this.Controls.Add(this.textBox_regpass);
            this.Controls.Add(this.textBox_reglogin);
            this.Controls.Add(this.panel_reg);
            this.Controls.Add(this.pictureBox_hidepass);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Registration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sign up";
            this.Load += new System.EventHandler(this.Registration_Load);
            this.panel_reg.ResumeLayout(false);
            this.panel_reg.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_showpass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_hidepass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.button_reg_back)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel_reg;
        private System.Windows.Forms.Label label_reg;
        private System.Windows.Forms.TextBox textBox_reglogin;
        private System.Windows.Forms.TextBox textBox_regpass;
        private System.Windows.Forms.Button button_regist;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox_showpass;
        private System.Windows.Forms.PictureBox pictureBox_hidepass;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button button_reg_exit;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox button_reg_back;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
    }
}