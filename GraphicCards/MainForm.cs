﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;



namespace GraphicCards
{



    public partial class MainForm : Form
    {
        private readonly checkuser user;
        Database database = new Database();

        public enum RowState
        {
            Modified,
            ModifiedNew,
            Deleted
        }


        int selectedRow;

        public MainForm(checkuser users)
        {
            user= users;
            InitializeComponent();
        }

        private void isAdmin()
        {
            
            button_admin.Enabled = user.isAdmin;
            button_newnote.Enabled = user.isAdmin;
            button_delete.Enabled = user.isAdmin;
            button_edit.Enabled = user.isAdmin;
            button_save.Enabled = user.isAdmin;
        }
       

        private void MainForm_Load(object sender, EventArgs e)
        {

            TableGrid tableGrid = new TableGrid(user);
            tableGrid.CreateColumns(dataGridView);
            tableGrid.RefreshDataGrid(dataGridView);
            dataGridView.ClearSelection();

            dataGridView.RowHeadersVisible = false;

            button_admin.Enabled = false;
            
            button_newnote.Enabled = false;
            button_delete.Enabled = false;
            button_edit.Enabled = false;
            button_save.Enabled =false;

            isAdmin();
        }

        private void dataGridView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            dataGridView.ClearSelection();
            ClearTextbox();
        }

        private void dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectedRow = e.RowIndex;

            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = dataGridView.Rows[selectedRow];

                textBox_id.Text = row.Cells[0].Value.ToString();
                textBox_model.Text = row.Cells[1].Value.ToString();
                textBox_manufacturer.Text = row.Cells[2].Value.ToString();
                textBox_count.Text = row.Cells[3].Value.ToString();
                textBox_price.Text = row.Cells[4].Value.ToString();
                textBox_vram.Text = row.Cells[5].Value.ToString();
                textBox_gpu.Text = row.Cells[6].Value.ToString();
                textBox_vvat.Text = row.Cells[7].Value.ToString();
            }
        }

        private void button_reload_Click(object sender, EventArgs e)
        {
            TableGrid tableGrid = new TableGrid(user);
            tableGrid.RefreshDataGrid(dataGridView);
            ClearTextbox();

        }

        private void button_newnote_Click(object sender, EventArgs e)
        {
            AddNote frm = new AddNote();
            frm.Show();
        }

        private void textBox_search_TextChanged(object sender, EventArgs e)
        {
                SearchAll(dataGridView);       
        }

        private void delete()
        {
            int index = dataGridView.CurrentCell.RowIndex;

            dataGridView.Rows[index].Visible = false;

            if (dataGridView.Rows[index].Cells[0].Value.ToString() != String.Empty)
            {
                dataGridView.Rows[index].Cells[8].Value = RowState.Deleted;
            }
            
        }


        private void UpdateDB()
        {
            database.openConnection();

            for (int index = 0; index < dataGridView.Rows.Count; index++)
            {
                RowState rowState = (RowState)dataGridView.Rows[index].Cells[8].Value;

                


                if (rowState == RowState.Deleted)
                {
                    int id = Convert.ToInt32(dataGridView.Rows[index].Cells[0].Value);
                    string sqlquery = $"delete from cards where id = {id}";

                    SqlCommand cmd = new SqlCommand(sqlquery, database.getConnection());

                    cmd.ExecuteNonQuery();

                }



                if (rowState == RowState.Modified)
                {
                    

                    int id = Convert.ToInt32(dataGridView.Rows[index].Cells[0].Value);
                    string modeln = dataGridView.Rows[index].Cells[1].Value.ToString();
                    string manuf = dataGridView.Rows[index].Cells[2].Value.ToString();
                    int count = Convert.ToInt32(dataGridView.Rows[index].Cells[3].Value);
                    int price = Convert.ToInt32(dataGridView.Rows[index].Cells[4].Value);
                    int vram = Convert.ToInt32(dataGridView.Rows[index].Cells[5].Value);
                    int gpu = Convert.ToInt32(dataGridView.Rows[index].Cells[6].Value);
                    int vvat = Convert.ToInt32(dataGridView.Rows[index].Cells[7].Value);


                    string changeQuery = $"update cards set model_name = '{modeln}',manufacturer = '{manuf}',count_of = '{count}',price = '{price}', vram = '{vram}', gpu = '{gpu}', usage = '{vvat}' where id = '{id}'";

                    SqlCommand command = new SqlCommand(changeQuery, database.getConnection());
                    command.ExecuteNonQuery();

                }




            }

            database.closeConnection();

        }


        private void SearchAll(DataGridView dgv)
        {
            TableGrid tableGrid = new TableGrid(user);

            dgv.Rows.Clear();

            string sqlquery = $"select * from cards where concat (id,model_name,manufacturer,count_of,price,vram,gpu,usage) like '%" + textBox_search.Text + "%' ";

            SqlCommand cmd = new SqlCommand(sqlquery, database.getConnection());

            database.openConnection();

            SqlDataReader read = cmd.ExecuteReader();

            while (read.Read())
            {
                tableGrid.ReadRow(dgv, read);
            }
            read.Close();
            database.closeConnection();
        }

        

        private void textBox_search_KeyPress(object sender, KeyPressEventArgs e)
        {
           

        }

        private void ClearTextbox()
        {
            textBox_id.Text = "";
            textBox_model.Text = "";
            textBox_manufacturer.Text = "";
            textBox_count.Text = "";
            textBox_vram.Text = "";
            textBox_gpu.Text = "";
            textBox_price.Text = "";
            textBox_vvat.Text = "";
        }

        private void EditNote()
        {
            var selectedRowIndex = dataGridView.CurrentCell.RowIndex;

            string id = textBox_id.Text;
            string model = textBox_model.Text;
            string manufac = textBox_manufacturer.Text;
            string count = textBox_count.Text;
            string vram = textBox_vram.Text;
            string gpu = textBox_gpu.Text;
            string vvat = textBox_vvat.Text;
            int price;

            if (dataGridView.Rows[selectedRowIndex].Cells[0].Value.ToString() != string.Empty)
            {
                if (int.TryParse(textBox_price.Text, out price))
                {
                    dataGridView.Rows[selectedRowIndex].SetValues(id, model, manufac, count, price, vram, gpu,vvat);
                    dataGridView.Rows[selectedRowIndex].Cells[8].Value = RowState.Modified;
                }
                else
                {
                    MessageBox.Show("Помилка!","Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }


        private void button_delete_Click(object sender, EventArgs e)
        {
            delete();
            ClearTextbox();
        }

        private void button_save_Click(object sender, EventArgs e)
        {
            UpdateDB();
        }

        private void button_edit_Click(object sender, EventArgs e)
        {
            EditNote();
            ClearTextbox();

        }

        private void button_main_exit_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
            
            
        }

        //Можливість переносити вікно форми
        bool mouseDown;
        private Point offset;

        private void panel_main_MouseDown(object sender, MouseEventArgs e)
        {
            offset.X = e.X;
            offset.Y = e.Y;
            mouseDown = true;
        }

        private void panel_main_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown == true)
            {
                Point currentScreenPos = PointToScreen(e.Location);
                Location = new Point(currentScreenPos.X - offset.X, currentScreenPos.Y - offset.Y);
            }
        }

        private void panel_main_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void button_main_back_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox_price_Enter(object sender, EventArgs e)
        {
            textBox_price.Text = "";
        }

        private void textBox_count_Enter(object sender, EventArgs e)
        {
            textBox_count.Text = "";
        }

        private void textBox_gpu_Enter(object sender, EventArgs e)
        {
            textBox_gpu.Text = "";
        }

        private void textBox_vvat_Enter(object sender, EventArgs e)
        {
            textBox_vvat.Text = "";
        }

        private void textBox_vram_Enter(object sender, EventArgs e)
        {
            textBox_vram.Text = "";
        }

        private void button_search_Click(object sender, EventArgs e)
        {
            
        }

        private void pictureBox_info_Click(object sender, EventArgs e)
        {
            InfoForm infoform = new InfoForm();
            infoform.Show();
            
        }

        

        private void button_admin_Click(object sender, EventArgs e)
        {
            AdminForm adminform = new AdminForm();
            adminform.Show();
        }
    }
}
