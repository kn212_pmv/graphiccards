﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GraphicCards
{
    public partial class AddNote : Form
    {
        public AddNote()
        {
            InitializeComponent();
        }

        Database database = new Database();


        private void button_addnewnote_Click(object sender, EventArgs e)
        {

            string model = textBox_model.Text;
            string manuf = textBox_manuf.Text;
            int count = int.Parse(textBox_count.Text);
            int price;
            int vram = int.Parse(textBox_vram.Text);
            int gpu = int.Parse(textBox_gpu.Text);
            int vvat = int.Parse(textBox_vvat.Text);

            if(int.TryParse(textBox_price.Text, out price))
            {
                database.openConnection();

                string sqlquery = $"insert into cards (model_name,manufacturer,count_of,price,vram,gpu,usage) values ('{model}', '{manuf}', '{count}', '{price}', '{vram}', '{gpu}', '{vvat}')";

                SqlCommand cmd = new SqlCommand(sqlquery, database.getConnection());

                cmd.ExecuteNonQuery();

                MessageBox.Show("Успішно!", "Успіх!", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else
            {
                MessageBox.Show("Помилка!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            database.closeConnection();
            this.Hide();

        }

        private void AddNote_Load(object sender, EventArgs e)
        {

        }


        //Кастомна кнопка закриття вікна
        private void button_add_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        //Можливість переносити вікно форми
        bool mouseDown;
        private Point offset;

        private void panel_add_MouseDown(object sender, MouseEventArgs e)
        {
            offset.X = e.X;
            offset.Y = e.Y;
            mouseDown = true;
        }

        private void panel_add_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown == true)
            {
                Point currentScreenPos = PointToScreen(e.Location);
                Location = new Point(currentScreenPos.X - offset.X, currentScreenPos.Y - offset.Y);
            }
        }

        private void panel_add_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void textBox_model_KeyPress(object sender, KeyPressEventArgs e)
        {
            //e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space);

        }

        private void textBox_manuf_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space);

        }

        private void textBox_count_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textBox_price_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textBox_vram_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textBox_gpu_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
