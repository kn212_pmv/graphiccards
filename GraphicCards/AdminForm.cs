﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GraphicCards
{
    public partial class AdminForm : Form
    {
        Database database = new Database();

        public AdminForm()
        {
            InitializeComponent();
        }

        private void AdminForm_Load(object sender, EventArgs e)
        {
            dataGridView2.RowHeadersVisible = false;
            CreateColumns();
            RefreshDataGrid();

        }


        private void CreateColumns()
        {
            dataGridView2.Columns.Add("id_user", "ID");
            dataGridView2.Columns.Add("login_user", "Логін");
            dataGridView2.Columns.Add("password_user", "Пароль");
            var checkcolumn = new DataGridViewCheckBoxColumn();
            checkcolumn.HeaderText = "isAdmin";
            dataGridView2.Columns.Add(checkcolumn);
        }

        private void ReadRow(IDataRecord record)

        {
            dataGridView2.Rows.Add(record.GetInt32(0), record.GetString(1), record.GetString(2), record.GetBoolean(3));
        }

        private void RefreshDataGrid()
        {
            dataGridView2.Rows.Clear();

            string sqlquery = $"SELECT * FROM registration";

            SqlCommand cmd = new SqlCommand(sqlquery,database.getConnection());

            database.openConnection();
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                ReadRow(reader);
            }

            reader.Close();

            database.closeConnection();

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView2.ClearSelection();
        }


        bool mouseDown;
        private Point offset;

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            offset.X = e.X;
            offset.Y = e.Y;
            mouseDown = true;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown == true)
            {
                Point currentScreenPos = PointToScreen(e.Location);
                Location = new Point(currentScreenPos.X - offset.X, currentScreenPos.Y - offset.Y);
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void button_main_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_newnote_Click(object sender, EventArgs e)
        {
            database.openConnection();

            for(int index = 0; index < dataGridView2.Rows.Count; index++)
            {
                var id = dataGridView2.Rows[index].Cells[0].Value.ToString();
                var isadmin = dataGridView2.Rows[index].Cells[3].Value.ToString();

                string sqlquery = $"UPDATE registration SET is_admin = '{isadmin}' WHERE id_user = '{id}'";

                SqlCommand cmd = new SqlCommand(sqlquery, database.getConnection());
                cmd.ExecuteNonQuery();

            }
            database.closeConnection();
            RefreshDataGrid();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            database.openConnection();

            var selectedRowIndex = dataGridView2.CurrentCell.RowIndex;

            var id = Convert.ToInt32(dataGridView2.Rows[selectedRowIndex].Cells[0].Value);

            string sqlquery = $"DELETE FROM registration WHERE id_user = '{id}'";

            SqlCommand cmd = new SqlCommand(sqlquery,database.getConnection());
            cmd.ExecuteNonQuery();

            database.closeConnection();
            RefreshDataGrid();
        }

        private void dataGridView2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            dataGridView2.ClearSelection();
        }
    }
}
