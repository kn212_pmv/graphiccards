﻿namespace GraphicCards
{
    partial class AddNote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddNote));
            this.textBox_model = new System.Windows.Forms.TextBox();
            this.textBox_manuf = new System.Windows.Forms.TextBox();
            this.textBox_count = new System.Windows.Forms.TextBox();
            this.textBox_price = new System.Windows.Forms.TextBox();
            this.textBox_vram = new System.Windows.Forms.TextBox();
            this.textBox_gpu = new System.Windows.Forms.TextBox();
            this.button_addnewnote = new System.Windows.Forms.Button();
            this.panel_add = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button_add_exit = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label_signup = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_vvat = new System.Windows.Forms.TextBox();
            this.panel_add.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox_model
            // 
            this.textBox_model.Location = new System.Drawing.Point(174, 104);
            this.textBox_model.Name = "textBox_model";
            this.textBox_model.Size = new System.Drawing.Size(172, 20);
            this.textBox_model.TabIndex = 0;
            this.textBox_model.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_model_KeyPress);
            // 
            // textBox_manuf
            // 
            this.textBox_manuf.Location = new System.Drawing.Point(174, 151);
            this.textBox_manuf.Name = "textBox_manuf";
            this.textBox_manuf.Size = new System.Drawing.Size(172, 20);
            this.textBox_manuf.TabIndex = 1;
            this.textBox_manuf.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_manuf_KeyPress);
            // 
            // textBox_count
            // 
            this.textBox_count.Location = new System.Drawing.Point(174, 200);
            this.textBox_count.Name = "textBox_count";
            this.textBox_count.Size = new System.Drawing.Size(172, 20);
            this.textBox_count.TabIndex = 2;
            this.textBox_count.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_count_KeyPress);
            // 
            // textBox_price
            // 
            this.textBox_price.Location = new System.Drawing.Point(174, 249);
            this.textBox_price.Name = "textBox_price";
            this.textBox_price.Size = new System.Drawing.Size(172, 20);
            this.textBox_price.TabIndex = 3;
            this.textBox_price.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_price_KeyPress);
            // 
            // textBox_vram
            // 
            this.textBox_vram.Location = new System.Drawing.Point(174, 296);
            this.textBox_vram.Name = "textBox_vram";
            this.textBox_vram.Size = new System.Drawing.Size(172, 20);
            this.textBox_vram.TabIndex = 4;
            this.textBox_vram.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_vram_KeyPress);
            // 
            // textBox_gpu
            // 
            this.textBox_gpu.Location = new System.Drawing.Point(174, 339);
            this.textBox_gpu.Name = "textBox_gpu";
            this.textBox_gpu.Size = new System.Drawing.Size(172, 20);
            this.textBox_gpu.TabIndex = 5;
            this.textBox_gpu.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_gpu_KeyPress);
            // 
            // button_addnewnote
            // 
            this.button_addnewnote.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(246)))), ((int)(((byte)(242)))));
            this.button_addnewnote.FlatAppearance.BorderSize = 0;
            this.button_addnewnote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_addnewnote.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.button_addnewnote.Location = new System.Drawing.Point(154, 432);
            this.button_addnewnote.Name = "button_addnewnote";
            this.button_addnewnote.Size = new System.Drawing.Size(172, 37);
            this.button_addnewnote.TabIndex = 6;
            this.button_addnewnote.Text = "Додати";
            this.button_addnewnote.UseVisualStyleBackColor = false;
            this.button_addnewnote.Click += new System.EventHandler(this.button_addnewnote_Click);
            // 
            // panel_add
            // 
            this.panel_add.Controls.Add(this.panel2);
            this.panel_add.Controls.Add(this.button_add_exit);
            this.panel_add.Controls.Add(this.pictureBox1);
            this.panel_add.Controls.Add(this.label_signup);
            this.panel_add.Location = new System.Drawing.Point(-3, -7);
            this.panel_add.Name = "panel_add";
            this.panel_add.Size = new System.Drawing.Size(496, 89);
            this.panel_add.TabIndex = 7;
            this.panel_add.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_add_MouseDown);
            this.panel_add.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_add_MouseMove);
            this.panel_add.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel_add_MouseUp);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(3, 90);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(496, 1);
            this.panel2.TabIndex = 18;
            // 
            // button_add_exit
            // 
            this.button_add_exit.FlatAppearance.BorderSize = 0;
            this.button_add_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_add_exit.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_add_exit.ForeColor = System.Drawing.Color.Black;
            this.button_add_exit.Location = new System.Drawing.Point(464, 7);
            this.button_add_exit.Name = "button_add_exit";
            this.button_add_exit.Size = new System.Drawing.Size(30, 31);
            this.button_add_exit.TabIndex = 15;
            this.button_add_exit.Text = "X";
            this.button_add_exit.UseVisualStyleBackColor = true;
            this.button_add_exit.Click += new System.EventHandler(this.button_add_exit_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(22, 26);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(49, 49);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // label_signup
            // 
            this.label_signup.AutoSize = true;
            this.label_signup.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_signup.ForeColor = System.Drawing.Color.Black;
            this.label_signup.Location = new System.Drawing.Point(139, 36);
            this.label_signup.Name = "label_signup";
            this.label_signup.Size = new System.Drawing.Size(235, 30);
            this.label_signup.TabIndex = 3;
            this.label_signup.Text = "Додавання запису";
            this.label_signup.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(93, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 21);
            this.label2.TabIndex = 8;
            this.label2.Text = "Модель:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(75, 151);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 21);
            this.label1.TabIndex = 9;
            this.label1.Text = "Виробник:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(90, 200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 21);
            this.label3.TabIndex = 10;
            this.label3.Text = "Кількість:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(119, 249);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 21);
            this.label4.TabIndex = 11;
            this.label4.Text = "Ціна:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(93, 296);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 21);
            this.label5.TabIndex = 12;
            this.label5.Text = "Пам\'ять:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(53, 339);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 21);
            this.label6.TabIndex = 13;
            this.label6.Text = "Частота GPU:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(55, 380);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 21);
            this.label7.TabIndex = 15;
            this.label7.Text = "Споживання:";
            // 
            // textBox_vvat
            // 
            this.textBox_vvat.Location = new System.Drawing.Point(174, 380);
            this.textBox_vvat.Name = "textBox_vvat";
            this.textBox_vvat.Size = new System.Drawing.Size(172, 20);
            this.textBox_vvat.TabIndex = 14;
            // 
            // AddNote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(201)))), ((int)(((byte)(202)))));
            this.ClientSize = new System.Drawing.Size(489, 501);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBox_vvat);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel_add);
            this.Controls.Add(this.button_addnewnote);
            this.Controls.Add(this.textBox_gpu);
            this.Controls.Add(this.textBox_vram);
            this.Controls.Add(this.textBox_price);
            this.Controls.Add(this.textBox_count);
            this.Controls.Add(this.textBox_manuf);
            this.Controls.Add(this.textBox_model);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddNote";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddNote";
            this.Load += new System.EventHandler(this.AddNote_Load);
            this.panel_add.ResumeLayout(false);
            this.panel_add.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_model;
        private System.Windows.Forms.TextBox textBox_manuf;
        private System.Windows.Forms.TextBox textBox_count;
        private System.Windows.Forms.TextBox textBox_price;
        private System.Windows.Forms.TextBox textBox_vram;
        private System.Windows.Forms.TextBox textBox_gpu;
        private System.Windows.Forms.Button button_addnewnote;
        private System.Windows.Forms.Panel panel_add;
        private System.Windows.Forms.Label label_signup;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button_add_exit;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_vvat;
    }
}