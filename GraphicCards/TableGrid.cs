﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace GraphicCards
{

    
    public class TableGrid : MainForm
    {
        Database database = new Database();

        public TableGrid(checkuser users) : base(users)
        {

        }


        public void CreateColumns(DataGridView dataGridView)
        {

            dataGridView.Columns.Add("id", "id");
            dataGridView.Columns.Add("model_name", "Модель");
            dataGridView.Columns.Add("manufacturer", "Виробник");
            dataGridView.Columns.Add("count_of", "Кількість");
            dataGridView.Columns.Add("price", "Ціна");
            dataGridView.Columns.Add("vram", "Кількість відеопам'яті");
            dataGridView.Columns.Add("gpu", "Частота GPU");
            dataGridView.Columns.Add("usage", "Споживання");
            dataGridView.Columns.Add("IsNew", String.Empty);
            
            dataGridView.Columns[0].Width = 18;
            dataGridView.Columns[1].Width = 200;

            dataGridView.Columns[8].Visible = false;
        }

        public void ReadRow(DataGridView dgv, IDataRecord record)
        {
            dgv.Rows.Add(record.GetInt32(0), record.GetString(1), record.GetString(2), record.GetInt32(3) + " шт.", record.GetInt32(4) + " грн.", record.GetInt32(5) + " ГБ", record.GetInt32(6) + " МГц",record.GetInt32(7) + " Вт", RowState.ModifiedNew);
            

        }


        public void RefreshDataGrid(DataGridView dgv)
        {
            dgv.Rows.Clear();

            string sqlquery = $"select * from cards";

            SqlCommand cmd = new SqlCommand(sqlquery, database.getConnection());

            database.openConnection();

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                ReadRow(dgv, reader);
            }
            reader.Close();
        }




    }
}
