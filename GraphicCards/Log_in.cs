﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GraphicCards
{
    public partial class Log_in : Form
    {
        
         Database database = new Database();

        public Log_in()
        {
            InitializeComponent();
        }


        private void Login_Load(object sender, EventArgs e)
        {
            textBox_pass.PasswordChar = '*';
            pictureBox_hidepass.Visible = false;
              
            
            textBox_login.MaxLength = 50;
            textBox_pass.MaxLength = 50;

            


        } 


        //Вхід у аккаунт
        private void button_log_Click(object sender, EventArgs e)
        {
            string login = textBox_login.Text;
            string pass = textBox_pass.Text;

            SqlDataAdapter adapter = new SqlDataAdapter();
            DataTable table = new DataTable();

            string querysql = $"select id_user, login_user, password_user, is_admin from registration where login_user = '{login}' and password_user = '{pass}'";

            SqlCommand cmd = new SqlCommand(querysql, database.getConnection());

            adapter.SelectCommand = cmd;
            adapter.Fill(table);
           
            if (table.Rows.Count == 1)
            {
                checkuser user = new checkuser(Convert.ToBoolean(table.Rows[0].ItemArray[3]));

                MainForm form = new MainForm(user);
                this.Hide();
                form.ShowDialog();
                this.Show();
            }
            else
                MessageBox.Show("Такого запису не існує!", "Запису не існує!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }



        //Перехід на форму реєстрації
        private void linkLabel_onreg_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Registration reg = new Registration();
            reg.Show();
            this.Hide();
        }




        //Показ паролю
        private void pictureBox_hidepass_Click(object sender, EventArgs e)
        {

            textBox_pass.UseSystemPasswordChar = false;
            pictureBox_showpass.Visible = true;
            pictureBox_hidepass.Visible = false;
            
        }

        private void pictureBox_showpass_Click(object sender, EventArgs e)
        {
            textBox_pass.UseSystemPasswordChar = true;
            pictureBox_showpass.Visible =false;
            pictureBox_hidepass.Visible = true;

           
        }





        //Кастомна кнопка закриття вікна
        
        private void button_log_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }



        //Placeholder для логіну та паролю форма Log_in
        private void textBox_login_Enter(object sender, EventArgs e)
        {
            if(textBox_login.Text == "Username")
            {
                textBox_login.Text = "";
            }
        }

        private void textBox_login_Leave(object sender, EventArgs e)
        {
            if (textBox_login.Text == "")
            {
                textBox_login.Text = "Username";
            }
        }

        private void textBox_pass_Enter(object sender, EventArgs e)
        {
            
            if (textBox_pass.Text == "Password")
            {
                textBox_pass.Text = "";
            }
        }

        private void textBox_pass_Leave(object sender, EventArgs e)
        {
            
            if (textBox_pass.Text == "")
            {
                textBox_pass.Text = "Password";
            }
        }




        //Можливість переносити вікно форми
        bool mouseDown;
        private Point offset;

        private void panel_login_MouseDown(object sender, MouseEventArgs e)
        {
            offset.X = e.X;
            offset.Y = e.Y;
            mouseDown = true;
        }

        private void panel_login_MouseMove(object sender, MouseEventArgs e)
        {
            if(mouseDown == true)
            {
                Point currentScreenPos = PointToScreen(e.Location);
                Location = new Point(currentScreenPos.X -offset.X, currentScreenPos.Y - offset.Y);
            }
        }

        private void panel_login_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        
    }
}
